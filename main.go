package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	productsinfo "go-pruduct-stores/products_info"

	_ "github.com/lib/pq"
)

type SelectedProducts struct {
	ID            int                     `json:"Id"`
	Name          string                  `json:"Name"`
	Model         string                  `json:"Model"`
	Price         float64                 `json:"Price"`
	Amount        int                     `json:"Amount"`
	Category_info productsinfo.Categories `json:"category"`
	Type_info     productsinfo.Types      `json:"type"`
	Stores_info   []productsinfo.Stores   `json:"Stores"`
}

func main() {
	cURL := "user=myusername password=:) dbname=product sslmode=disable"
	db, err := sql.Open("postgres", cURL)
	if err != nil {
		fmt.Println("Error while trying to connect to db", err)
	}

	for _, product := range productsinfo.Products_list {
		var store_ids []int
		for _, store := range product.Stores_list {
			err := db.QueryRow("INSERT INTO stores(name) VALUES($1) returning id", store.Name).Scan(&store.ID)
			if err != nil {
				fmt.Println("ERROR WHILE INSERTING TO STORES", err)
				return
			}
			for _, addres := range store.Addresses_list {
				_, err := db.Exec(`INSERT INTO addresses(district, street, store_id)
				VALUES($1, $2, $3)`, addres.District, addres.Street, store.ID)
				if err != nil {
					fmt.Println(err)
					return
				}
			}
			store_ids = append(store_ids, store.ID)
		}
		err = db.QueryRow(`INSERT INTO products(name, category_id, type_id, model, price, amount)
		VALUES($1, $2, $3, $4, $5, $6) returning id`, product.Name, product.CategoryID, product.TypeID, product.Model, product.Price, product.Amount).Scan(&product.ID)
		if err != nil {
			fmt.Println(err)
		}
		for _, id := range store_ids {
			_, err := db.Exec(`INSERT INTO stores_products VALUES($1, $2)`, id, product.ID)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}
	fmt.Println("Succes")

	allProducts := SelectAll(db)
	jsonInfo, err := json.MarshalIndent(allProducts, "", "   ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonInfo))
}

func SelectAll(db *sql.DB) []SelectedProducts {
	res := []SelectedProducts{}
	allProducts := `select * from products;`
	rows, err := db.Query(allProducts)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		sel_product := SelectedProducts{}
		temp_prod := productsinfo.Products{}
		err := rows.Scan(&sel_product.ID, &sel_product.Name, &temp_prod.CategoryID, &temp_prod.TypeID, &sel_product.Model, &sel_product.Price, &sel_product.Amount)
		if err != nil {
			panic(err)
		}
		sel_product.Type_info = SelectType(db, temp_prod.TypeID)
		sel_product.Category_info = SelectCategory(db, temp_prod.CategoryID)
		sel_product.Stores_info = SelectStore(db, sel_product.ID)
		res = append(res, sel_product)
	}

	return res
}

func SelectCategory(db *sql.DB, id int) productsinfo.Categories {
	res := productsinfo.Categories{}
	err := db.QueryRow(`select * from categories where id = $1`, id).Scan(&res.ID, &res.Name)
	if err != nil {
		panic(err)
	}
	return res
}
func SelectType(db *sql.DB, id int) productsinfo.Types {
	res := productsinfo.Types{}
	err := db.QueryRow(`select * from types where id=$1`, id).Scan(&res.ID, &res.Name)
	if err != nil {
		panic(err)
	}
	return res
}

func SelectStore(db *sql.DB, product_id int) []productsinfo.Stores {
	res := []productsinfo.Stores{}
	rows, err := db.Query(`select id, name from stores inner join stores_products on id = store_id where product_id = $1;`, product_id)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		temp := productsinfo.Stores{}
		err := rows.Scan(&temp.ID, &temp.Name)
		if err != nil {
			fmt.Println(err)
		}
		rows2, err := db.Query(`select id, district, street from addresses where store_id=$1`, temp.ID)
		if err != nil {
			panic(err)
		}
		allAddresses := []productsinfo.Addresses{}
		for rows2.Next() {
			tempAddr := productsinfo.Addresses{}
			err := rows2.Scan(&tempAddr.ID, &tempAddr.District, &tempAddr.Street)
			if err != nil {
				panic(err)
			}
			allAddresses = append(allAddresses, tempAddr)
		}
		temp.Addresses_list = allAddresses
		res = append(res, temp)
		rows2.Close()
	}

	rows.Close()
	return res
}
