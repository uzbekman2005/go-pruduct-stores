package productsinfo

type Types struct {
	ID   int    `json:"type_id"`
	Name string `json:"name_type"`
}

type Categories struct {
	ID   int    `json:"category_id"`
	Name string `json:"name_category"`
}

type Stores struct {
	ID             int         `json:"store_id"`
	Name           string      `json:"name_store"`
	Addresses_list []Addresses `json:"Addresses"`
}

type Addresses struct {
	ID       int    `json:"address_id"`
	District string `json:"district"`
	Street   string `json:"street"`
	StoreID  int    
}

type Products struct {
	ID          int
	Name        string
	CategoryID  int
	TypeID      int
	Model       string
	Price       float64
	Amount      int
	Stores_list []Stores
}

var Products_list = []Products{
	{
		Name:       "Meat",
		CategoryID: 1,
		TypeID:     1,
		Model:      "Qozoq",
		Price:      8,
		Amount:     100,
		Stores_list: []Stores{
			{
				Name: "Karzinka",
				Addresses_list: []Addresses{
					{
						District: "Olmazor",
						Street:   "Olmazor highway",
					},
					{
						District: "Chilonzor",
						Street:   "Mirzo Ulug'bek highway",
					},
					{
						District: "Yunusobod",
						Street:   "Alisher Navoiy highway",
					},
				},
			},
			{
				Name: "Havas",
				Addresses_list: []Addresses{
					{
						District: "Olmazor",
						Street:   "Olmazor Havas",
					},
					{
						District: "Chilonzor",
						Street:   "Mirzo Ulug'bek Havas",
					},
					{
						District: "Yunusobod",
						Street:   "Alisher Navoiy Havas",
					},
				},
			},
		},
	},
	{
		Name:       "Cobalt",
		CategoryID: 2,
		TypeID:     3,
		Model:      "4 LTZ",
		Price:      14000,
		Amount:     40,
		Stores_list: []Stores{
			{
				Name: "TOSH Avto salon",
				Addresses_list: []Addresses{
					{
						District: "Olmazor",
						Street:   "Olmazor avot",
					},
					{
						District: "Chilonzor",
						Street:   "Mirzo Ulug'bek avot",
					},
					{
						District: "Yunusobod",
						Street:   "Alisher Navoiy avot",
					},
				},
			},
			{
				Name: "Karshi Avto salon",
				Addresses_list: []Addresses{
					{
						District: "Qarshi",
						Street:   "Katta ko'cha",
					},
					{
						District: "Qarshi",
						Street:   "Amir Temur halqa yo'li",
					},
				},
			},
		},
	},
	{
		Name:       "IPhone",
		CategoryID: 4,
		TypeID:     4,
		Model:      "13 pro",
		Price:      1200,
		Amount:     20,
		Stores_list: []Stores{
			{
				Name: "Malika",
				Addresses_list: []Addresses{
					{
						District: "Yunusobod",
						Street:   "Unknown",
					},
				},
			},
			{
				Name: "Media Park",
				Addresses_list: []Addresses{
					{
						District: "Chilanzar",
						Street:   "Qatartol ko'cha",
					},
					{
						District: "Yunusobod",
						Street:   "Katta halqa yo'li",
					},
				},
			},
		},
	},
	{
		Name:       "Gucci",
		CategoryID: 2,
		TypeID:     2,
		Model:      "Redline",
		Price:      120,
		Amount:     30,
		Stores_list: []Stores{
			{
				Name: "Gipermarket",
				Addresses_list: []Addresses{
					{
						District: "Uchtepa",
						Street:   "Unknown",
					},
				},
			},
		},
	},
}
